package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
)

func main() {
	reader := bufio.NewReader(os.Stdin)

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	dataSize := 0
	_, err := fmt.Fscan(reader, &dataSize)
	if err != nil {
		fmt.Println(err)
		return
	}

	for index := 0; index < dataSize; index++ {
		const dataPartSize int = 10
		dataPart := make([]int, dataPartSize)

		for index := 0; index < dataPartSize; index++ {
			var val int
			_, err = fmt.Fscan(reader, &val)
			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Println(err)
				return
			}
			dataPart[index] = val
		}
		sort.Ints(dataPart)

		var (
			one   int
			two   int
			three int
			four  int
		)
		for _, val := range dataPart {
			switch {
			case val == 1:
				one++
			case val == 2:
				two++
			case val == 3:
				three++
			case val == 4:
				four++
			}
		}

		if one == 4 && two == 3 && three == 2 && four == 1 {
			out.WriteString("YES\n")
		} else {
			out.WriteString("NO\n")
		}
	}
}
