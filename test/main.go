package test

import (
	"fmt"
)

func test(p int) {
	fmt.Println(p)
}

func main() {
	val := 10

	test(val)
	test(&val)
}
