package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
)

func main() {
	file, err := os.Open("01")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file) //os.Stdin)

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	dataSize := 0
	_, err = fmt.Fscan(reader, &dataSize)
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		var dataPartSize int
		_, err := fmt.Fscan(reader, &dataPartSize)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}

		dataPart := make([]int, dataPartSize)

		for index := 0; index < dataPartSize; index++ {
			var val int
			_, err = fmt.Fscan(reader, &val)
			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Println(err)
				return
			}
			dataPart[index] = val
		}

		//fmt.Println(dataPart)


		}

		//out.WriteString("hello")
	}
}
