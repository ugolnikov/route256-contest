package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"regexp"
)

func main() {
	file, err := os.Open("10")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	dataSize := 0
	_, err = fmt.Fscan(reader, &dataSize)
	if err != nil {
		fmt.Println(err)
		return
	}

	for index := 0; index < dataSize; index++ {

		part, err := getPart(reader)
		if err != nil {
			return
		}

		//part - содержит
		//Задача С

		//два типа номеров
		//ВХОД : <ADDAA|ADAA>...<> - множество
		//Проверка даолжна выявлять соответствие строки заданной форме
		//ВЫВОД: "-" - !check, result - check

		//case len(part) < 3:  false
		//case 3 < len(part) < 50+1 :

		check := false
		result := make([]string, 2, 10)

		switch {
		case len(part) < 3:
			check = false
		case 3 < len(part) && len(part) < 50+1:
			result, err = validation(part)
			if err != nil {
				return
			}
			if len(result) != 0 {
				check = true
			}
		}

		printResult(check, result, out)
	}
}

func validation(str string) ([]string, error) {
	//номер ADDAA: A
	//номер ADAA : B
	//case - строка имеет вид <A|B>+
	//case - строка невалидна

	regex, err := regexp.Compile("([[:alpha:]][[:digit:]][[:digit:]][[:alpha:]][[:alpha:]]|[[:alpha:]][[:digit:]][[:alpha:]][[:alpha:]])")
	if err != nil {
		fmt.Println(err)
		return []string{}, err
	}
	result := regex.FindAllString(str, -1)
	if result == nil {
		return result, errors.New("result is nil")
	}

	var matchedLength int
	for _, str := range result {
		matchedLength += len(str)
	}

	if matchedLength != len(str) {
		result = []string{}
	}

	return result, nil
}

func getPart(reader *bufio.Reader) (string, error) {
	var part string
	_, err := fmt.Fscan(reader, &part)
	if err != nil {
		fmt.Println(err)
		return "", errors.New("sobaka, ne rabotaet. Ne chitaet this string")
	}
	return part, nil
}

func printResult(check bool, result []string, out *bufio.Writer) {
	if check {
		for index, str := range result {
			if index == len(result)-1 {
				out.WriteString(str)
				out.WriteString("\n")
			} else {
				out.WriteString(str)
				out.WriteString(" ")
			}
		}
	} else {
		out.WriteString("-\n")
	}
	defer out.Flush()
}
