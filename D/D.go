package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
)

func main() {
	file, err := os.Open("09")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file) //os.Stdin)

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	dataSize := 0
	_, err = fmt.Fscan(reader, &dataSize)
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		var dataPartSize int
		_, err := fmt.Fscan(reader, &dataPartSize)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}

		scores := make([]int, dataPartSize)

		for index := 0; index < dataPartSize; index++ {
			var val int
			_, err = fmt.Fscan(reader, &val)
			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Println(err)
				return
			}
			scores[index] = val
		}

		//scores - набор входных значений
		//Данные - это набор целых чисех

		//sortedScores - отсортированные scores
		//grades - набор присвоенных мест с сохранением исходных индексов
		grades := make([]int, dataPartSize)

		sortedScores := make([]int, dataPartSize)
		copy(sortedScores, scores)
		sort.Ints(sortedScores)

		currentGrade := 1
		for index, value := range sortedScores {
			if index > 0 {
				if sortedScores[index] == sortedScores[index-1] {
					continue
				}

				if sortedScores[index] == sortedScores[index-1]+1 {
					setGradesByValue(currentGrade, value, scores, grades)
				} else {
					currentGrade = index + 1
					setGradesByValue(currentGrade, value, scores, grades)
				}
			} else {
				setGradesByValue(currentGrade, value, scores, grades)
			}
		}

		for index, value := range grades {
			if index > 0 {
				out.WriteString(" ")
				out.WriteString(strconv.Itoa(value))
			} else {
				out.WriteString(strconv.Itoa(value))
			}
		}
		out.WriteString("\n")
	}
}

func setGradesByValue(grade int, value int, scores []int, grades []int) {
	indexes := getIndexesByValue(value, scores)
	setGrades(grade, indexes, grades)
}

func setGrades(grade int, indexes []int, grades []int) {
	for _, value := range indexes {
		grades[value] = grade
	}
}

func getIndexesByValue(val int, source []int) []int {
	return searchInts(val, source)
}

func searchInts(val int, source []int) []int {
	var indexes []int
	for index, value := range source {
		if val == value {
			indexes = append(indexes, index)
		}
	}
	return indexes
}
