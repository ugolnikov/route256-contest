package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	file, err := os.Open("01")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file) //os.Stdin)

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	dataSize := 0
	_, err = fmt.Fscan(reader, &dataSize)
	if err != nil {
		fmt.Println(err)
		return
	}

	for index := 0; index < dataSize; index++ {
		var documentSize int
		_, err := fmt.Fscan(reader, &documentSize)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}

		var setSize, dataPartSize int
		//чтение первой строки тестовых данных
		_, err = fmt.Fscan(reader, &setSize)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}
		_, err = fmt.Fscan(reader, &dataPartSize)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}
		//чтение второй строки тестовых данных
		appointments := make([]int, dataPartSize)

		for index := 0; index < dataPartSize; index++ {
			var val int
			_, err = fmt.Fscan(reader, &val)
			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Println(err)
				return
			}
			appointments[index] = val
		}

		//логика решения

		//структура: (appointments []int) =(find)> (map[appointment int] sign string)
		//sortedAppointments []ing

		//case тройка и просветы
		//	если просвет только с одной стороны, тогда Х
		//	с двух сторон

		//case есть двойка и есть просветы по бокам
		// 	с одной стороны
		//	с двух сторон

		//case если набор имеет 4 и больше дубликатов, тогда X
		//case если падряд идут две тройки дубликатов, тогда X
		//case если подряд идут три двойки дубликатов, тогда Х
		//case если набор не имеет дубликатов, тогда все 0
	}
}

func indexOf(value int, slice []int) (index int, ok bool) {

}

//мне нужно сопоставлять позиции исходного набора с примененными операциями(map)

//мне нужно разработать логику сдвигов
//найти пропуски и сдвигать рядом стоящие дубликаты и числа за ним в сторону ближайшего пропуска

func isMissingValue(value int) bool {
	//формула суммы арифметической прогрессии S = ((a1 + an)*n)/2
}

func check(appointments []int) bool {
	//todo проверка на количество дубликатов
	//если набор имеет 4 и больше дубликатов, тогда X
	//если падряд идут две тройки дубликатов, тогда X
	//если подряд идут три двойки дубликатов, тогда Х
	//если набор не имеет дубликатов, тогда все 0
}

func inc(value int) int {

}

func dec(value int) int {

}
