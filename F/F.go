package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	file, err := os.Open("12")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file) //os.Stdin)

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	dataSize := 0
	_, err = fmt.Fscan(reader, &dataSize)
	if err != nil {
		fmt.Println(err)
		return
	}

	for index := 0; index < dataSize; index++ {
		var documentSize int
		_, err := fmt.Fscan(reader, &documentSize)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}

		var parts string
		_, err = fmt.Fscan(reader, &parts)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}

		//обработка тестовой строки
		flags := make([]bool, documentSize+1)
		ok, err := checkInputRanges(documentSize, parts)
		if !ok {
			fmt.Println("error")
		}
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		ranges, err := splitRanges(parts)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		for _, val := range ranges {
			err := markFlags(flags, val.ToString())
			if err != nil {
				fmt.Println(err.Error())
				return
			}
		}

		ranges = getUnmarkedFlagsAsRanges(flags)
		for index, value := range ranges {
			if index != 0 {
				out.WriteString(",")
				out.WriteString(value.ToString())
			} else {
				out.WriteString(value.ToString())
			}
		}
		out.WriteString("\n")
		out.Flush()
	}
}

func getUnmarkedFlagsAsRanges(flags []bool) []Range {
	ranges := []Range{}
	for index := 1; index < len(flags); {
		if flags[index] {
			index++
			continue
		}
		var tempRange Range
		if !flags[index] {
			tempRange.Begin = index
		}
		for ; index < len(flags) && !flags[index]; index++ {
			tempRange.End = index
		}
		ranges = append(ranges, tempRange)
	}
	return ranges
}

func markFlags(flags []bool, inputRange string) error {
	tempRange, err := stringToRange(inputRange)
	if err != nil {
		return err
	}

	if tempRange.Begin == tempRange.End {
		flags[tempRange.Begin] = true
	}

	for index := tempRange.Begin; index <= tempRange.End; index++ {
		flags[index] = true
	}
	return nil
}

type Range struct {
	Begin int
	End   int
}

func (*Range) isBelong(val int, inputRange string) (bool, error) {
	target, err := splitRanges(inputRange)
	if err != nil {
		return false, err
	}

	if val >= target[0].Begin && val <= target[0].End {
		return true, nil
	}

	return false, nil
}

func stringToRange(inputRange string) (Range, error) {
	result := Range{}

	regex, err := regexp.Compile("(\\d+)")
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	found := regex.FindAllString(inputRange, -1)
	if found == nil {
		return result, errors.New("result is nil")
	}

	switch {
	case len(found) == 1:
		val, err := strconv.Atoi(found[0])
		if err != nil {
			return result, err
		}
		result = Range{Begin: val, End: val}
	case len(found) == 2:
		begin, err := strconv.Atoi(found[0])
		if err != nil {
			return result, err
		}
		end, err := strconv.Atoi(found[1])
		if err != nil {
			return result, err
		}
		result = Range{Begin: begin, End: end}
	}
	return result, nil
}

func splitRanges(input string) ([]Range, error) {
	ranges := []Range{}
	regex, err := regexp.Compile("(\\d+-\\d+)|(\\d+)")
	if err != nil {
		fmt.Println(err)
		return ranges, err
	}
	result := regex.FindAllString(input, -1)
	if result == nil {
		return ranges, errors.New("result is nil")
	}

	for _, str := range result {
		tempRange, err := stringToRange(str)
		if err != nil {
			return ranges, err
		}
		ranges = append(ranges, tempRange)
	}
	return ranges, nil
}

func checkInputRanges(documentSize int, input string) (bool, error) {
	if strings.HasPrefix(input, ",") {
		return false, nil //,1,2,3-5 (каждая запятая должна разделять два элемента, начинать список она не может),
	}
	if strings.HasSuffix(input, ",") {
		return false, nil //1-5, (каждая запятая должна разделять два элемента, заканчивать список она не может)
	}
	if strings.HasSuffix(input, ",,") {
		return false, nil //1,,3 (две запятые не могут идти подряд)
	}

	regex, err := regexp.Compile("(\\d+-\\d+-\\d+)")
	if err != nil {
		fmt.Println(err)
		return false, err
	}
	result := regex.FindAllString(input, -1)
	if result != nil {
		return false, errors.New("bad form of input data")
	}

	for index := 0; index < len(input); {
		if unicode.IsDigit(int32(input[index])) {
			str := input[index:len(input)]
			reader := strings.NewReader(str)
			value := 0
			_, err := fmt.Fscan(reader, &value)
			if err != nil {
				return false, err
			}

			if value > documentSize {
				return false, nil //7-9 (девятую страницу нельзя послать на печать)
			}

			for ; index < len(input) && unicode.IsDigit(int32(input[index])); index++ {
			}
		} else {
			index++
		}
	}

	return true, nil
}

func (r Range) ToString() string {
	var result string
	if r.Begin == r.End {
		result = strconv.Itoa(r.Begin)
	} else {
		result = strconv.Itoa(r.Begin)
		result += "-"
		result += strconv.Itoa(r.End)
	}
	return result
}
