package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
)

func main() {
	file, err := os.Open("02")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file) //os.Stdin)

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	dataSize := 0
	_, err = fmt.Fscan(reader, &dataSize)
	if err != nil {
		fmt.Println(err)
		return
	}

	for index := 0; index < dataSize; index++ {
		var dataPartSize int
		_, err := fmt.Fscan(reader, &dataPartSize)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			return
		}

		request := make([]string, dataPartSize)

		for index := 0; index < dataPartSize; index++ {
			var val string
			_, err := fmt.Fscan(reader, &val)
			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Println(err)
				return
			}
			request[index] = val
		}

		//request - это строка букв
		//проверка на схожесть чувствительная к капитализации
		//case они полностью равны
		//case

		fmt.Println(request)

		request = removeDuplicates(request)
		fmt.Println(request)

		sort.Strings(request)
		fmt.Println(request)

		preparedNames := make([]PreparedName, len(request))

		for index, str := range request {
			preparedNames[index] = prepareSymbols(str)
			fmt.Println(preparedNames[index])
		}

		counter := 0
		if len(preparedNames) != 0 {
			counter = 1
		}

		for index := 0; index < len(preparedNames); {
			for j := index + 1; j < len(preparedNames); j++ {
				if isSameNames(preparedNames[index], preparedNames[j]) {
					if index+1 < len(preparedNames) {
						index = j
					}
				} else {
					if index+1 < len(preparedNames) {
						index = j
					}
					counter++
					break
				}
			}
			if index+1 == len(preparedNames) {
				break
			}
		}

		out.WriteString(strconv.Itoa(counter))
		out.WriteString("\n")
		fmt.Println("-------------------")

		//out.WriteString("\n")
	}
}

func isSameNames(nameA PreparedName, nameB PreparedName) bool {
	if len(nameA) != len(nameB) {
		return false
	}

	for index := 0; index < len(nameA); index++ {
		if !isSameSymbols(nameA[index], nameB[index]) {
			return false
		}
	}
	return true
}

func isSameSymbols(symbolA PreparedSymbol, symbolB PreparedSymbol) bool {
	check := false

	if symbolA.symbol == symbolB.symbol {
		switch {
		case symbolA.size == symbolB.size:
			check = true
		case symbolA.size == 1 && symbolB.size > 1:
			check = false
		case symbolA.size > 1 && symbolB.size == 1:
			check = false
		case Abs(symbolA.size-symbolB.size) > 0:
			check = true
		}
	}

	return check
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

type PreparedSymbol struct {
	symbol int32
	size   int
}

type PreparedName []PreparedSymbol

func prepareSymbols(str string) []PreparedSymbol {
	var (
		name       []PreparedSymbol
		prepared   PreparedSymbol
		lastSymbol int32
		size       int
	)

	for index, currentSymbol := range str {
		if index > 0 {
			if currentSymbol == lastSymbol {
				name[len(name)-1].size++
			} else {
				size = 1
				prepared = PreparedSymbol{currentSymbol, size}
				name = append(name, prepared)
				lastSymbol = currentSymbol
			}
		} else {
			lastSymbol = currentSymbol
			prepared.symbol = currentSymbol
			prepared.size++
			name = append(name, prepared)
		}
	}

	return name
}

func removeDuplicates(request []string) []string {
	filter := map[string]int{}

	for _, item := range request {
		filter[item] = 0
	}

	result := make([]string, len(filter))

	index := 0
	for key, _ := range filter {
		result[index] = key
		index++
	}
	return result
}
